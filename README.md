# oso-k8s-helm-hands_on

## Hands-on of Kubernetes (K8S) / OpenShift (OSO) / Helm

---
Pre-requirements:

* Minikube - Kubernetes 1.x - https://minikube.sigs.k8s.io/docs/start/
* Minishift - OpenShift 3.x - https://docs.okd.io/3.11/minishift/getting-started/preparing-to-install.html
* Helm - 3.x - https://helm.sh/docs/intro/install/
* Kubectl  - https://kubernetes.io/docs/tasks/tools/
* oc (In case you do not install Minishift) - https://github.com/openshift/origin/releases/tag/v3.11.0
  
Extras
* CRC (CodeReady Container) - OpenShift 4.x - https://developers.redhat.com/blog/2019/09/05/red-hat-openshift-4-on-your-laptop-introducing-red-hat-codeready-containers/
* Helm 2 (Tiller in OSO) - https://blog.openshift.com/getting-started-helm-openshift/
* K8S Tools - https://kubernetes.io/docs/tasks/tools/
---
### K8S Cheat Sheet
* K8S Cheat Sheet - https://kubernetes.io/docs/reference/kubectl/cheatsheet/
* `kubectl api-resources` - List of K8S/OSO Resources

### K8S/OSO Objects
* API - https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/
* Deployments - https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
* Init containers - https://kubernetes.io/docs/concepts/workloads/pods/init-containers/ 
* Liveness and Readiness Probe - https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ | https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#configure-probes
* Services - https://kubernetes.io/docs/concepts/services-networking/service/
* ConfigMap - https://kubernetes.io/docs/concepts/configuration/configmap/
* Secrets - https://kubernetes.io/docs/concepts/configuration/secret/ | https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-environment-variables
* Ingress - https://kubernetes.io/docs/concepts/services-networking/ingress/ 
* Ingress Minikube - https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/
* Route - https://docs.openshift.com/container-platform/3.11/rest_api/route_openshift_io/route-route-openshift-io-v1.html
* Container lifecycle and hooks - https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/
* Termination of Pods - https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-termination


### Helm
* Using helm - ttps://helm.sh/docs/intro/using_helm/
* Charts - https://helm.sh/docs/topics/charts/
* Chart Template Guide Main page - https://helm.sh/docs/chart_template_guide/
* Flow control (ifs/ elses ...) - https://helm.sh/docs/chart_template_guide/control_structures/
* Functions and Pipelines  - https://helm.sh/docs/chart_template_guide/functions_and_pipelines/  
* Function list - https://helm.sh/docs/chart_template_guide/function_list/
* Sub-charts - https://helm.sh/docs/chart_template_guide/subcharts_and_globals/
* Helm 3 vc 2 - https://helm.sh/docs/faq/#changes-since-helm-2
* Automatically Roll Deployments - https://helm.sh/docs/howto/charts_tips_and_tricks/#automatically-roll-deployments
  

#### Minikube

Tutorial

`minikube start` <br>
`minikube addons list` <br>
`minikube addons enable ingress` # If you are using Minikube in Linux or Windows (without WSL2) <br>
`see commands to enable ingress in Linux under Wsl2` # If you are using linux in Win10 by Wsl2 check in Extra by 'Minikube with Linux via WSL2' and do not forget to use `minikube tunnel` command explained in the web link <br>
`minikube dashboard` <br>
`kubectl create namespace <namespace_name>` <br>
`kubectl apply -f k8s-oso-core/. -f k8s/. -n webbox`  * If any error, check in Extra by 'Error when running apply command' <br> 
`kubectl get ingress` <br>
`kubectl delete -f k8s-oso-core/. -f k8s/. -n webbox` <br>
`curl http://192.168.49.2/hostname -H 'Host: webbox.com'`  <br>
`curl http://webbox.com/hostname` * Check in Extra by 'Route host to ip' how to configure it <br>
`minikube delete` <br>

Extra

- Commands <br>
`minikube stop` <br>
`kubectl config` <br>
`kubectl config current-context` <br>
`kubectl config set-context webbox` <br>
`kubectl config get-contexts` <br>
`kubectl config set-context --current --namespace=webbox` <br>
`kubectl get ingress -o wide` <br>
`minikube addons list` <br>
`minikube addons enable ingress` <br>
 - Error when running apply command -> `kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission` - "Error from server (InternalError): error when creating "ingress.yml": Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": an error on the server ("") has prevented the request from succeeding" <br>
 - Minikube with Linux via WSL2 -> https://hellokube.dev/posts/configure-minikube-ingress-on-wsl2/ 
 - Route host to ip ->
   - https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file
   - to get the IP execute `kubectl get ingress`
   - If you are running Minikube in linux under Wsl2, maybe you will consider configuring the Linux and Windows depending of where you are accessing it. From linux terminal or Browser/Postman under Windows for example 

#### Minishift

Tutorial

`minishift start` <br>
`minishift console` <br>
`oc login -u developer` <br>
`oc new-project <project_name>` (or use UI) <br>
`oc project <project_name>` <br>
`oc apply -f k8s-oso-core/. -f oso/. -n webbox` <br>
`oc get routes -n webbox` <br>
`curl -k https://webbox-webbox.192.168.42.6.nip.io/hostname` <br>
`oc delete -f k8s-oso-core/. -f oso/. -n webbox` <br>
`minishift delete` <br>

Extra

`minishift stop` <br>
`minishift delete` <br>
`minishift status` <br>
`minishift oc-env` (In case you do not have oc installed) <br>
`oc project` <br>
`oc log <pod_name>` <br>
`oc log <pod_name> -c <container_name>` (Ideal to check logs from init container)


### Helm Commands

#### Chart

Dry-run

`helm --dry-run --debug -n webbox upgrade webbox --install ./helm/chart/webbox`

Template

`helm template ./helm/chart/webbox --output-dir ./helm/zout`

Install

`helm -n webbox upgrade webbox --install ./helm/chart/webbox` <br>
`helm -n webbox upgrade webbox --install ./helm/chart/webbox --set global.platform=k8s` <br>
`kubectl get ingress -n webbox` <br>
`curl http://192.168.49.2/hostname -H 'Host: webbox.com'` <br>

Delete

`helm -n webbox uninstall webbox` <br>
`helm -n webbox delete webbox` <br>
`helm delete oso-demo --purge` (Helm 2)

#### Sub-Chart

Dry-run

`helm --dry-run --debug -n webbox upgrade webbox --install ./helm/sub-chart/webbox`

Template

`helm template ./helm/sub-chart/webbox --output-dir ./helm/zout` <br>
`helm template ./helm/sub-chart/webbox --output-dir ./helm/zout -f ./helm/sub-chart/values/custom.yaml` <br>
`helm template ./helm/sub-chart/webbox --output-dir ./helm/zout -f ./helm/sub-chart/values/custom.yaml --set webbox-1.replicas=5`


`helm -n webbox upgrade webbox --install ./helm/sub-chart/webbox`<br>
`helm -n webbox upgrade webbox --install ./helm/sub-chart/webbox -f ./helm/sub-chart/values/custom.yaml`<br>
`helm -n webbox upgrade webbox --install ./helm/sub-chart/webbox -f ./helm/sub-chart/values/custom.yaml --set webbox-1.replicas=5`

Install

`helm -n webbox upgrade webbox --install ./helm/sub-chart/webbox` <br>
`helm -n webbox upgrade webbox --install ./helm/sub-chart/webbox --set global.platform=k8s` <br>
`kubectl get ingress -n webbox` <br>
`curl http://192.168.49.2/hostname -H 'Host: webbox-1.com'` <br>
`curl http://192.168.49.2/hostname -H 'Host: webbox-2.com'` <br>

Delete

`helm -n webbox delete webbox` <br>
`helm -n webbox uninstall webbox` <br>
`helm delete oso-demo --purge` (Helm 2)

#### Extra commands

`helm ls -n webbox` <br>
`helm get all webbox -n webbox`

###### Example of calls

`while true; do curl -X GET http://IP/hostname; sleep 0; done` <br>
`while true; do curl -k -X GET https://webbox-1-webbox.192.168.42.6.nip.io/hostname; sleep 0; done` <br>
`curl http://192.168.49.2/hostname -H 'Host: webbox.com'`
`curl -k https://webbox-1-webbox.192.168.42.6.nip.io/hostname`



---
Links

https://gitlab.com/eduardopax/webbox

https://medium.com/trendyol-tech/graceful-shutdown-of-spring-boot-applications-in-kubernetes-f80e0b3a30b0

https://iceburn.medium.com/use-ingress-in-minikube-ae5e87ea858c

 

